# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Save favourite feature
- added on save favourite faeture to sanity 
- added login_user(name) helper method


### Changed


### Fixed



## [0.1.0] - 2016-12-20
### Added





