module PageObjects

class LoginPage  < SitePrism::Page

  set_url "https://test.net/default.php"
  
  section :modules,Modules, ".menuModule"
  section :search,Search, ".menuSearch"
  section :create,Create, ".menuCreate"

  element :loginButton, "#loginBtn"
  element :requestAccess,"#reqAccsBtn"
  element :askForSupport, "#askSuprtBtn"
  
  
  def login
    loginButton.click
  end

  def requestAccess
    requestAccess.click
  end

  def requestSupport
    askForSupport.click
  end

  def loginpagecheck
    page.has_button?("#loginBtn")
    if(find("#loginBtn").disabled?)
     raise "Blocker Login button disabled"
     end
    page.has_button?("#reqAccsBtn")
    if(find("#reqAccsBtn").disabled?)
      raise "Blocker:Request Access button disabled"
    end
    page.has_button?("#askSuprtBtn")
    if(find("#askSuprtBtn").disabled?)
      raise "Blocker:Request help button disabled"
    end

  end
  
  
  def setNetwork(serial,network)
    page.find(:xpath , "(//input[@id='network'])[#{serial}]").set(network)
    Name.click
  end



  def checknetDisabled
    if( page.find("#net", :match => :first)['disabled'].should == "true")
    else
    raise "net field should be disabled"
    end
  end
  
  
  


end
end
