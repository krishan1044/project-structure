## move file to sections , does nt belong here and load

module PageObjects

  class Modules < SitePrism::Section


    def goto_search
      ele = find('.menuModule')
      page.driver.browser.action.click(ele.native).perform
      page.driver.browser.action.click_and_hold(ele.native).release.perform
      find("#Link").click
     # sleep(1)
    end

    def goto_user
      ele = find('.menuModule')
      page.driver.browser.action.click(ele.native).perform
      page.driver.browser.action.click_and_hold(ele.native).release.perform
      find("#ConfGenLink").click
        #    sleep(1)
    end


  end

  
  
  class Usertools < SitePrism::Section

    def goto_diagnostic
      find("#_diagnostics").click
        #   sleep(1)
    end

    def goto_about
      find("#AboutLink").click
        #  sleep(1)
    end

    def goto_help
      find("#HelpLink").click
        #  sleep(1)
    end

    def logout
      find("#logout").click
        #   sleep(1)
    end

  end



end






=begin
# method to have a common base page and this prevents inclusion of section on each page individually else need to go with normal page object site prism implementation

  class BasePage < SitePrism::Page
    section :navigation, Navigation, ".navigation"
  end
end

module PageObjects
  class Dashboard < BasePage
    …
  end
end

module PageObjects
  class Navigation < SitePrism::Section
    element :component_one, ".component_one"
    element :componont_two, ".component_two"

    def goto_component_one
      component_one.click
    end

    def goto_component_two
      component_two.click
    end
  end
=end






=begin
BACKUP
  class Menu < SitePrism::Section


element :modules,".menuModule"
element :search,".menuSearch"
element :admin,".menuAdmin"
element :history,".menuHistory"
element :create,".menuCreate"
element :bulkEdit,".menuBulkEdit"
element :import,".menuImport"
element :ipAddress,".menuIPAddress"


def goto_modules
  ele = find('.menuModule')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_search
  ele = find('.menuSearch')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_admin
  ele = find('.menuAdmin')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_history
  ele = find('.menuHistory')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_create
  ele = find('.menuCreate')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_bulkEdit
  ele = find('.menuBulkEdit')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_import
  ele = find('.menuImport')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

def goto_ipaddress
  ele = find('.menuIPAddress')
  page.driver.browser.action.click_and_hold(ele.native).release.perform
end

end
=end


=begin
class Create < SitePrism::Section

  def goto_park
    find("#ParkLink").click
  end

  def goto_stationgroup
    find("#StationGroupLink").click
  end

  def goto_station
    find("#StationLink").click
  end

  def goto_functiongroup
    find("#FunctionGroupLink").click
  end

  def goto_unit
    find("#UnitLink").click
  end


end
=end


=begin
class Modules < SitePrism::Section
  # has to be changed
  #elements :park, "body > div.row > div.col-md-1.sideBarWrapper > ul > li.dropdown.open > ul > li:nth-child(1) > a"

  def goto_ipplan
    find("#IPPlanLink").click
  end

  def goto_crsp
    find("#ConfGenLink").click
  end

  def goto_confgen
    find("#CRSPLink").click
  end

  def goto_pss
    find("#PSSlink").click
  end

  def goto_prime
    find("#PrimeLink").click
  end

end
=end



=begin
class BulkEdit < SitePrism::Section
  # has to be changed
  # elements :park, "body > div.row > div.col-md-1.sideBarWrapper > ul > li.dropdown.open > ul > li:nth-child(1) > a"



    def goto_demo
      #park.click
      # park.select
     # find(:xpath,"/html/body/div[2]/div[1]/ul/li[3]/ul/li[1]/a").click
    end






end

=end






=begin
class Import < SitePrism::Section
  # has to be changed
  # elements :park, "body > div.row > div.col-md-1.sideBarWrapper > ul > li.dropdown.open > ul > li:nth-child(1) > a"

=begin



    def goto_park
      #park.click
      # park.select
      find(:xpath,"/html/body/div[2]/div[1]/ul/li[3]/ul/li[1]/a").click
    end



=end








=begin

class IpAddress < SitePrism::Section

end


class History < SitePrism::Section

end
=end


=begin
class Admin < SitePrism::Section
  # has to be changed
  # elements :park, "body > div.row > div.col-md-1.sideBarWrapper > ul > li.dropdown.open > ul > li:nth-child(1) > a"

  def goto_globalgrant
    find("#GlobalGrantsLink").click
  end

  def goto_regionalgrant
    find("#ReginalGrantsLink").click
  end

  def goto_emailgroups
    find("#EmailGroupsLink").click
  end

  def goto_banner
    find("#BannerLink").click
  end

  def goto_country
    find("#CountryLink").click
  end

  def goto_region
    find("#RegionLink").click
  end

  def goto_unittypelist
    find("#UnitTypeListLink").click
  end
  def goto_unittypemodify
    find("#UnitTypeModifyLink").click
  end
  def goto_turbinelist
    find("#TurbineListLink").click
  end
  def goto_turbinemodify
    find("#TurbineModifyLink").click
  end
  def goto_supernet
    find("#SupernetLink").click
  end

  def goto_customer
    find("#CustomerLink").click
  end
  def goto_topology
    find("#TopologyLink").click
  end
  def goto_function
    find("#FunctionLink").click
  end

end
=end

=begin

class Usertools < SitePrism::Section
  # has to be changed
  # elements :park, "body > div.row > div.col-md-1.sideBarWrapper > ul > li.dropdown.open > ul > li:nth-child(1) > a"

  def goto_diagnostic
    find("#_diagnostics").click
  end

  def goto_about
    find("#AboutLink").click
  end

  def goto_help
    find("#HelpLink").click
  end

  def goto_logout
    find("#logout").click
  end

end
=end





