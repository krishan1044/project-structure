

module PageObjects    #:nodoc:
  def visit_page(name, args = {}, &block)
    build_page(name).tap do |page|
      page.load(args)
      block.call page if block
    end
  end

  def on_page(name, args = {}, &block)
    build_page(name).tap do |page|
      expect(page).to be_displayed(args)
      block.call page if block
    end
  end

  def build_page(name)
    name = name.to_s.camelize if name.is_a? Symbol
    Object.const_get("PageObjects::#{name}").new
  end

# Used for user login
#
#*accepts:users first name (all lowercase) type:string
#*throws user credential file not found error if user not found
#*user credential file must be present at "D:\UserFile\username" folder
#*user batch file(userfirstname.bat) must be present at D:\UserFile

  



  def login_as(name)
  # user_admin = `D:\\UserFile\\#{name}.bat`
    stdout, stdeerr, status = Open3.capture3("D:\\UserFile\\#{name}.bat")
    if(stdeerr!="")
      raise stdeerr
    end

  end
  
 end 