#Scenario:Admin should be able to edit user detail
Given (/^I navigate to application page$/) do
   login_as("user")
   visit_page("MyApplication")
end

When (/^I click on login button$/) do
   on_page("LoginPage").login
end

Then (/^I should see application homepage$/) do
 on_page("LoginPage").loginpagecheck
end


When (/^I visit application page$/)do
  visit_page("LoginPage")
end

And (/^I fill user data$/)do
  on_page("HomePage").search.goto_usermenu
  on_page("ModifyPage").selectCountry(@country)
  on_page("ModifyPage").selectProvince(@provinceedit)
  on_page("ModifyPage").save
end

Then (/^I should see login page$/)do
  
  expect(on_page("WfiLoginPage")).to be_displayed            ## dont want to do validation like this here
 