
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'cucumber'
require 'pry'
require 'byebug'
require 'win32ole'
require 'capybara-screenshot/cucumber'
require 'rubygems'
require 'zip'
require 'open3'

require_relative '../../test_commons/all_page_objects'

Capybara.default_driver = :selenium

Capybara.register_driver :selenium do |app|
  profile = Selenium::WebDriver::Firefox::Profile.new
  profile.native_events = true
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.javascript_driver = :chrome


Before '@fullscreen' do
 page.driver.browser.manage.window.maximize
 Capybara::Screenshot.autosave_on_failure = true
 Capybara.save_path = "screenshots"

end



scenario_times = {}

Around() do |scenario, block|
  start = Time.now
  block.call

  if scenario.is_a? Cucumber::Core::Ast::ScenarioOutline
    thing = scenario.scenario_outline
  else
    thing = scenario
  end
  scenario_times["#{thing.feature.file}::#{thing.name}"] = Time.now - start


end

at_exit do
  max_scenarios = scenario_times.size > 500 ? 500 : scenario_times.size
  puts "------------- Top #{max_scenarios} slowest scenarios -------------"
  sorted_times = scenario_times.sort { |a, b| b[1] <=> a[1] }
  sorted_times[0..max_scenarios - 1].each do |key, value|
    puts "#{value.round(2)}  #{key}"
  end
end



World(PageObjects)

SitePrism.configure do |config|
  config.use_implicit_waits = true
end







